{
	/* ##############################################################
	   ##############################################################
	   
	   Determines if Wrath of the Hive can be applied to players outside
	   the Bumblezone dimension when they pick up Honey blocks, take honey
	    from Filled Porous Honey blocks, or drink Honey Bottles.
	*/
	"allowWrathOfTheHiveOutsideBumblezone": false,
	/* ##############################################################
	   ##############################################################
	   
	   Show the orangish particles when you get Wrath of the Hive
	   after you angered the bees in the Bumblezone dimension.
	*/
	"showWrathOfTheHiveParticles": true,
	/* ##############################################################
	   ##############################################################
	   
	   Turn off or on the ability to get Wrath of the Hive effect.
	   
	   The effect gets applied when you pick up Honey blocks, take
	   honey from Filled Porous Honeycomb Blocks, or drink a
	   Honey Bottle inside the Bumblezone dimension.
	   Basically, bees become REALLY angry.
	   
	   In addition, the bees can see you through walls and will have 
	   speed, absorption, and strength effects applied to them.
	   
	   Will also affect the bee's aggression toward bears in the dimension.
	   Note: Peaceful mode will always override the bee aggressive setting.
	*/
	"aggressiveBees": true,
	/* ##############################################################
	   ##############################################################
	   
	   How far away the bee can be to become angry and hunt you down if
	   you take their honey from the Bumblezone dimension.
	   
	   Will also affect the bee's aggression range toward bears in the dimension.
	*/
	"aggressionTriggerRadius": 64,
	/* ##############################################################
	   ##############################################################
	   
	   How long bees will keep their effects for (speed, absorption, strength).
	   
	   Note: This is in ticks. 20 ticks = 1 second. And bee's anger will remain.
	   Only the boosts given to the bees will be gone.
	*/
	"howLongWrathOfTheHiveLasts": 350,
	/* ##############################################################
	   ##############################################################
	   
	   How long entities will keep Protection of the Hive effect after feeding bees
	   or Brood Blocks. Bees will attack anyone that damages someone with the effect.
	   
	   Note: This is in ticks. 20 ticks = 1 second.
	*/
	"howLongProtectionOfTheHiveLasts": 500,
	/* ##############################################################
	   ##############################################################
	   
	   How fast bees move along the ground (Not while flying).
	   You will see this a lot when bees are about to attack
	   you, they tend to touch the floor and the speed boost
	   makes them dash forward at you. Set this to higher for
	   faster dash attacks from bees.
	*/
	"speedBoostLevel": 1,
	/* ##############################################################
	   ##############################################################
	   
	    How much extra health bees get that always instantly regenerates.
	    This means you need to deal more damage than the extra health gives
	    order to actually damage the bee's real health bar.
	    
	    For example, Absorpton 1 here makes bees get 4 extra padding of hearts.
	    Your attacks need to deal 4 1/2 or more damage to actuall be able to
	    kill the bee. This means using Bane of Arthropod 5 is needed to kill bees
	    if you set the absorption to a higher value like 2 or 3.
	    If you set this to like 5 or something, bees may be invicible! Game over.
	*/
	"absorptionBoostLevel": 1,
	/* ##############################################################
	   ##############################################################
	   
	   How strong the bees attacks become.
	   (5 or higher will instant kill you without armor).
	*/
	"strengthBoostLevel": 3,
	/* ##############################################################
	   ##############################################################
	   
	   How rare Bee Dungeons are. Higher numbers means more rare.
	   Default rate is 1. Setting to greater than 1000 will disable Bee Dungeons.
	*/
	"beeDungeonRarity": 1,
	/* ##############################################################
	   ##############################################################
	   
	   How rare Spider Infested Bee Dungeons are. Higher numbers means more rare.
	   Default rate is 8. Setting to greater than 1000 will disable Spider Infested Bee Dungeons.
	*/
	"spiderInfestedBeeDungeonRarity": 8,
	/* ##############################################################
	   ##############################################################
	   
	   How rare are Spider/Cave Spider Spawners in Spider Infested Bee Dungeons.
	   0 is no spawners, 1 is maximum spawners, and default is 0.2f
	*/
	"spawnerRateSpiderBeeDungeon": 0.20000000298023224,
	/* ##############################################################
	   ##############################################################
	   
	    Determines how the coordinates gets translated when entering
	   and leaving the Bumblezone. The default ratio is 10 which means
	   for every block you traverse in the dimension, it is equal to
	   traveling 10 blocks in the Overworld. For comparison, the Nether
	   has a 8 to 1 ratio with the Overworld. 
	   
	   The scaling of coordinates will take into account other dimension's
	   coordinate ratios so it'll work for any dimension correctly.
	   
	   Note: Changing this in an already made world will change where Bee Nests will
	   take you in the dimension and exiting will place you in a different spot too. 
	    ONLY FOR TELEPORTATION MODE 1 AND 3.
	*/
	"movementFactor": 10,
	/* ##############################################################
	   ##############################################################
	   
	   How bright the fog is in the Bumblezone dimension. 
	   This will always affect the fog whether you have the 
	   day/night cycle on or off.
	   
	   The brightness is represented as a percentage so if the 
	   cycle is off, 0 will be pitch black, 50 will be half as 
	   bright, 100 will be normal orange brightness, and 
	   100000 will be white. When the cycle is on, 0 will be 
	   but will not be completely black during daytime.
	*/
	"fogBrightnessPercentage": 110.0,
	/* ##############################################################
	   ##############################################################
	   
	   Makes leaving The Bumblezone dimension always places you back
	    at the Overworld regardless of which dimension you originally 
	   came from. Use this option if this dimension becomes locked in  
	   with another dimension so you are stuck teleporting between the 
	   two and cannot get back to the Overworld
	*/
	"forceExitToOverworld": false,
	/* ##############################################################
	   ##############################################################
	   
	    Should exiting The Bumblezone always try and place you 
	    above sealevel in the target dimension? (Will only look 
	    for beehives above sealevel as well when placing you) 
	    ONLY FOR TELEPORTATION MODE 1 AND 3.
	*/
	"seaLevelOrHigherExitTeleporting": true,
	/* ##############################################################
	   ##############################################################
	   
	    If an identifier of a block is specified here,
	     then teleporting to Bumblezone will need that block under
	    the Bee Nest/Beehive you threw the Enderpearl at.
	    
	    Example: minecraft:emerald_block will require you to place an
	    Emerald Block under the Bee Nest/Beehive and then throw an
	    Enderpearl at it to teleport to Bumblezone dimension.
	    
	    By default, no identifieris specified so any
	    block can be under the Bee Nest/Beehive to teleport to dimension.
	*/
	"requiredBlockUnderHive": "",
	/* ##############################################################
	   ##############################################################
	   
	    If requiredBlockUnderHive has a block specified and this config
	     is set to true, then player will get a warning if they throw 
	    an Enderpearl at a Bee Nest/Beehive but the block under it is 
	    not the correct required block. It will also tell the player what 
	    block is needed under the Bee Nest/Beehive to teleport to the dimension.
	*/
	"warnPlayersOfWrongBlockUnderHive": true,
	/* ##############################################################
	   ##############################################################
	   
	    Will a Beenest generate if no Beenest is  
	    found when leaving The Bumblezone dimension.
	    
	    ONLY FOR TELEPORTATION MODE 1.
	*/
	"generateBeenest": true,
	/* ##############################################################
	   ##############################################################
	   
	    Which mode of teleportation should be used when 
	    leaving The Bumblezone dimension. 
	    
	    Mode 1: Coordinates will be converted to the other 
	    dimension's coordinate scale and the game will look for
	    a Beenest/Beehive at the new spot to spawn players at. 
	    If none is found, players will still be placed at the spot.
	    
	    Mode 2: Will always spawn players at the original spot 
	    in the non-BZ dimension where they threw the Enderpearl 
	    at a Beenest/Beehive. Will place air if the spot is now filled 
	    with solid blocks. 
	    
	    Mode 3: Coordinates will be converted to the other 
	    dimension's coordinate scale and the game will look for
	    a Beenest/Beehive at the new spot to spawn players at. 
	    If none is found, players will spawn at the original spot
	    in the non-BZ dimension where they threw the Enderpearl 
	    at a Beenest/Beehive. Will place air if the spot is now filled 
	    with solid blocks. 
	*/
	"teleportationMode": 1,
	/* ##############################################################
	   ##############################################################
	   
	    Should Dispensers always drop the Glass Bottle when using specific 
	    bottle items on certain The Bumblezone blocks? 
	    
	    Example: Using Honey Bottle to feed Honeycomb Brood Blocks will grow the 
	    larva and have a Glass Bottle to either drop or put back into Dispenser. 
	*/
	"dispensersDropGlassBottles": false
}